package com.pietruh.recipes.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.sql.DataSource;

import com.pietruh.recipes.dao.api.factory.ModelFactory;
import com.pietruh.recipes.dao.api.interfaces.CategoryDao;
import com.pietruh.recipes.dao.api.model.Category;
import com.pietruh.recipes.dao.api.model.Recipe;

@Stateless
@Remote(CategoryDao.class)
public class CategoryDaoBean implements CategoryDao {

	@Resource(mappedName = "java:jboss/datasources/RecipesDS")
	private DataSource dataSource;

	@Inject
	private ModelFactory modelFactory;
//@formatter:off
	
	private static final String SELECT_SQL=
			"SELECT r.id r_id, r.user_id r_user_id, r.name r_name, r.description r_description, r.cooking_time r_cooking_time, r.published r_published, r.image r_image, "
			+ "rc.id rc_id, rc.category_id rc_category_id, rc.recipe_id rc_recipe_id, "
			+ "c.id c_id, c.user_id c_user_id, c.name c_name, c.description c_description "
			+ "FROM recipe r "
			+ "LEFT JOIN recipe_category rc ON r.id=rc.recipe_id "
			+ "FULL JOIN category c ON c.id=rc.category_id "; 
//	@formatter:on
	@Override
	public List<Category> getUserCategories(int userId) {

		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			StringBuilder sb = new StringBuilder();
			sb.append(CategoryDaoBean.SELECT_SQL);
			sb.append("WHERE r.user_id=? OR c.user_id=? ");
			sb.append("ORDER BY c.id");
			PreparedStatement statement = connection.prepareStatement(sb.toString());

			statement.setInt(1, userId);
			statement.setInt(2, userId);

			statement.execute();

			ResultSet set = statement.getResultSet();
			List<Category> categories = new ArrayList<>();
			while (set.next()) {
				Category c = modelFactory.createCategory();
				c.setId(set.getInt("c_id"));
				c.setDescription(set.getString("c_description"));
				c.setName(set.getString("c_name"));
				List<Recipe> recipes = new ArrayList<>();
				for (; c.getId() == set.getInt("rc_category_id"); set.next()) {
					Recipe r = modelFactory.createRecipe();
					r.setId(String.valueOf(set.getInt("r_id")));
					r.setName(set.getString("r_name"));
					// r.setDescription(set.getString("r_description"));
					recipes.add(r);
				}
				c.setRecipes(recipes);
				categories.add(c);
			}
			return categories;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException();
		} finally {
			closeConnection(connection);
		}
	}

	@Override
	public Category getUserCategory(int categoryId, int userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Category addCategory(Category category, int userId) {
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			PreparedStatement statement = connection
					.prepareStatement("INSERT INTO category(userId, name, description) VALUES (?, ?, ?)",
							Statement.RETURN_GENERATED_KEYS);

			statement.setInt(1, userId);
			statement.setString(2, category.getName());
			statement.setString(3, category.getDescription());
			if (statement.executeUpdate() != 0) {
				ResultSet set = statement.getGeneratedKeys();
				if (set.next()) {
					category.setId((int) set.getLong(1));
				} else {
					throw new RuntimeException();
				}
			} else {
				throw new RuntimeException();
			}
			return category;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException();
		} finally {
			closeConnection(connection);
		}

	}

	@Override
	public void deleteCategory(int categoryId) {
		// TODO Auto-generated method stub

	}

	private void closeConnection(Connection connection) {
		try {
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
