package com.pietruh.recipes.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.sql.DataSource;

import com.pietruh.recipes.dao.api.factory.ModelFactory;
import com.pietruh.recipes.dao.api.interfaces.RecipeDao;
import com.pietruh.recipes.dao.api.model.Ingredient;
import com.pietruh.recipes.dao.api.model.Recipe;
import com.pietruh.recipes.dao.api.model.Weight;

@Stateless
@Remote(RecipeDao.class)
public class RecipeDaoBean implements RecipeDao {

	// private final String SQL =
	// "SELECT id,user_id, name, description, cooking_time WHERE user_id=?;";

	@Resource(mappedName = "java:jboss/datasources/RecipesDS")
	private DataSource dataSource;

	@Inject
	private ModelFactory modelFactory;

	@Override
	public List<Recipe> getUserRecipes(int userId) {
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			PreparedStatement statement = connection.prepareStatement(this.getSelectSql() + " WHERE r.user_id=? ");
			statement.setInt(1, userId);
			statement.execute();
			ResultSet rs = statement.getResultSet();
			return getRecipses(rs);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		} finally {
			this.closeConnection(connection);
		}
	}

	private String getSelectSql() {
		// @formatter:off
		return "SELECT r.id AS r_id, r.user_id AS r_user_id, r.name AS r_name, r.description AS r_description, r.cooking_time AS r_cooking_time, r.image AS r_image, r.published AS r_published, "
				+ "ri.id AS ri_id, ri.name AS ri_name, ri.amount AS ri_amount, ri.weight AS ri_weight "
				+ "FROM recipe AS r LEFT JOIN recipe_ingredient AS ri ON r.id=ri.recipe_id ";
		// @formatter:on
	}

	private List<Recipe> getRecipses(ResultSet rs) throws SQLException {
		Map<Integer, Recipe> recipesMap = new HashMap<>();
		while (rs.next()) {
			Integer id = rs.getInt("id");
			if (recipesMap.containsKey(id)) {
				Recipe recipe = recipesMap.get(id);
				recipe.getIngredients().add(this.getIngredientFromResult(rs));
			} else {
				Recipe recipe = getRecipeFromResult(rs);
				recipesMap.put(id, recipe);
			}
		}
		return new ArrayList<Recipe>(recipesMap.values());
	}

	@SuppressWarnings(value = { "Api impelementation" })
	private Recipe getRecipeFromResult(ResultSet rs) throws SQLException {
		Recipe recipe = modelFactory.createRecipe();
		recipe.setId(Integer.toString(rs.getInt("r_id")));
		recipe.setName(rs.getString("r_name"));
		// recipe.setDescription(rs.getString("r_description"));
		recipe.setCookingTime(rs.getInt("r_cooking_time"));
		recipe.setPhoto(null);
		recipe.setPublished(rs.getBoolean("r_published"));

		Ingredient ingredient = this.getIngredientFromResult(rs);
		List<Ingredient> ingredients = new ArrayList<Ingredient>();
		ingredients.add(ingredient);
		recipe.setIngredients(ingredients);
		return recipe;
	}

	private Ingredient getIngredientFromResult(ResultSet rs) throws SQLException {
		Ingredient ingr = modelFactory.creatIngridient();
		ingr.setId(rs.getInt("ri_id"));
		ingr.setName(rs.getString("ri_name"));
		ingr.setAmount(rs.getInt("ri_amount"));
		if (rs.wasNull())
			ingr.setAmount(-1);
		ingr.setWeight(Weight.getByString(rs.getInt("ri_weight")));
		if (rs.wasNull())
			ingr.setWeight(null);
		return ingr;
	}

	private void closeConnection(Connection connection) {
		try {
			if (connection != null)
				connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public List<Recipe> getUserFavouriteRecipes(int userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Recipe getRecipe(String recipeId) {
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			PreparedStatement statement = connection.prepareStatement(this.getSelectSql() + " WHERE r.id = ?");
			statement.setInt(1, Integer.parseInt(recipeId));
			statement.execute();
			ResultSet rs = statement.getResultSet();
			Recipe recipe = null;
			if (rs.next()) {
				recipe = this.getRecipeFromResult(rs);
			} else {
				throw new RuntimeException("No recipe found");
			}
			while (rs.next()) {
				recipe.getIngredients().add(this.getIngredientFromResult(rs));
			}
			return recipe;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (connection != null)
				closeConnection(connection);
		}

		return null;
	}

	@SuppressWarnings(value = { "Api impelementation", "unused" })
	private Recipe getRecipe(ResultSet set) throws SQLException {
		Recipe recipe = modelFactory.createRecipe();
		List<Ingredient> ingredients = null;
		if (set.next()) {
			ingredients = new ArrayList<>();
			recipe.setId(String.valueOf(set.getInt("r_id")));
			recipe.setName(set.getString("r_name"));
			// recipe.setDescription(set.getString("r_description"));
			Ingredient ingredient = modelFactory.creatIngridient();
			ingredient.setAmount(set.getInt("ri_amount"));
			ingredient.setId(set.getInt("ri_id"));
			ingredient.setName(set.getString("ri_name"));
			ingredient.setWeight(Weight.getByString(set.getInt("ri_weight")));
			ingredients.add(ingredient);
		}
		while (set.next()) {
			Ingredient ingredient = modelFactory.creatIngridient();
			ingredient.setAmount(set.getInt("ri_amount"));
			ingredient.setId(set.getInt("ri_id"));
			ingredient.setName(set.getString("ri_name"));
			ingredient.setWeight(Weight.getByString(set.getInt("ri_weight")));
			ingredients.add(ingredient);
		}
		recipe.setIngredients(ingredients);
		return recipe;
	}

	@Override
	public Recipe addRecipe(Recipe recipe) {
		Connection connection = null;
		try {
			connection = dataSource.getConnection();

			try (PreparedStatement statement = connection.prepareStatement("", Statement.RETURN_GENERATED_KEYS)) {
				//
				statement.executeUpdate();
				ResultSet set = statement.getGeneratedKeys();
				if (set.next()) {
					long id = set.getLong(1);
					try (PreparedStatement batchStatment = connection.prepareStatement("")) {
						for (Ingredient ing : recipe.getIngredients()) {
							batchStatment.setString(1, ing.getName());
							batchStatment.setInt(2, ing.getAmount());
							batchStatment.setInt(3, Weight.getWeightId(ing.getWeight()));

							batchStatment.addBatch();
						}
					}
					statement.executeBatch();
				} else {

				}

			}

		} catch (SQLException e) {

		} finally {
			closeConnection(connection);
		}

		return null;
	}

	@Override
	public void deleteRecipe(String recipeId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateRecipe(Recipe recipe) {
		// TODO Auto-generated method stub

	}
}
