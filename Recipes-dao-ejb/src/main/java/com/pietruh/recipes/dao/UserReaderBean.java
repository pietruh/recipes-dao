package com.pietruh.recipes.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.sql.DataSource;

import com.pietruh.recipes.dao.api.factory.ModelFactory;
import com.pietruh.recipes.dao.api.interfaces.UserReader;
import com.pietruh.recipes.dao.api.model.User;

@Stateless
@LocalBean
public class UserReaderBean implements Serializable, UserReader {

	private static final long serialVersionUID = -8639114540982404210L;

	private final String SELECT_USER = "SELECT id, name, email, password FROM user_account WHERE name=?;";
	@Resource(mappedName = "java:jboss/datasources/RecipesDS")
	private DataSource dataSource;

	@Inject
	private ModelFactory modelFactory;

	@Override
	public User getUser(String name) {
		User user = null;
		try {
			Connection connection = dataSource.getConnection();
			PreparedStatement statement = connection
					.prepareStatement(SELECT_USER);
			statement.setString(1, name);
			statement.execute();

			ResultSet resultSet = statement.getResultSet();
			if (resultSet.next()) {
				user = modelFactory.createUser();
				user.setId(resultSet.getInt("id"));
				user.setName(resultSet.getString("name"));
				user.setPassword(resultSet.getString("password"));
				user.setEmail(resultSet.getString("email"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}
}
