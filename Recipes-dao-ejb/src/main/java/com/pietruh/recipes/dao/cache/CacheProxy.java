package com.pietruh.recipes.dao.cache;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

public class CacheProxy {

	public CacheProxy() {
		CacheManager manager = CacheManager.create();
		manager.addCache("test");
		// TODO Auto-generated constructor stub
	}

	private void createCache() {
		CacheManager.getInstance().addCache("xyz"); // creates a cache called
													// xyz.

		Cache xyz = CacheManager.getInstance().getCache("xyz");

		xyz.put(new Element("key", "value"));
		Element e = xyz.get("key");
		String p = (String) e.getObjectValue();
	}
}
