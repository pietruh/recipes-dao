package com.pietruh.recipes.dao.producers;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;

import com.pietruh.recipes.dao.api.factory.ModelFactory;
import com.pietruh.recipes.dao.api.impl.factory.DefaultModelFactory;

public class ModelFactoryProducer {

	@Named
	@Produces
	@ApplicationScoped
	private ModelFactory createModelFactory() {
		return new DefaultModelFactory();
	}
}
