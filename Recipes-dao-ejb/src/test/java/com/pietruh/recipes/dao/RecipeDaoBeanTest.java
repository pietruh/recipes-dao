package com.pietruh.recipes.dao;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.EnterpriseArchive;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.pietruh.recipes.dao.api.factory.ModelFactory;
import com.pietruh.recipes.dao.api.model.Recipe;
import com.pietruh.recipes.dao.api.model.User;

@RunWith(Arquillian.class)
public class RecipeDaoBeanTest {

	@Deployment
	public static Archive<?> deployment() {

		// @formatter:off
		File[] files = Maven.resolver().loadPomFromFile("pom.xml")
				.importRuntimeDependencies().resolve().withoutTransitivity()
				.as(File.class);
		
		JavaArchive jar = ShrinkWrap.create(JavaArchive.class,"test.jar")
				.addAsManifestResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"));
		// @formatter:on
		EnterpriseArchive ear = ShrinkWrap.create(EnterpriseArchive.class,
				"test.ear").addAsLibraries(files);
		ear.addAsLibraries(jar);
		return ear;
	}

	@Inject
	ModelFactory modelFactory;

	@Inject
	RecipeDaoBean recipeReader;
	
	@Test
	public void recipeForUserTest() throws Exception {
		User user = modelFactory.createUser();
		user.setName("test");
		user.setId(1);
		List<Recipe> recipes= recipeReader.getUserRecipes(user.getId());
	}

}
