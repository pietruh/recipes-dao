package com.pietruh.recipes.dao;

import static org.junit.Assert.assertEquals;

import java.io.File;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.EnterpriseArchive;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.pietruh.recipes.dao.api.factory.ModelFactory;
import com.pietruh.recipes.dao.api.interfaces.UserReader;
import com.pietruh.recipes.dao.api.model.User;

@RunWith(Arquillian.class)
// @RunAsClient
public class UserReaderTest {

	@Deployment
	public static Archive<?> createTestArchive() {

		// @formatter:off
		File[] files = Maven.resolver().loadPomFromFile("pom.xml")
				.importRuntimeDependencies().resolve().withoutTransitivity()
				.as(File.class);

		JavaArchive jar = ShrinkWrap.create(JavaArchive.class,"test.jar")
				.addClass(UserReaderBean.class)
				.addClass(UserReaderTest.class)
				.addAsManifestResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"));
		// @formatter:on
		EnterpriseArchive ear = ShrinkWrap.create(EnterpriseArchive.class,
				"test.ear").addAsLibraries(files);
		ear.addAsLibraries(jar);
		return ear;
	}

	@Inject
	UserReader reader;
	@Inject
	ModelFactory modelFactory;

	private User expectedUser;

	@Test
	public void getAllUsers() throws Exception {
		this.expectedUser = modelFactory.createUser();
		expectedUser.setEmail("test@onet.pl");
		expectedUser.setId(1);
		expectedUser.setName("test");
		expectedUser.setPassword("test");
		User user = reader.getUser("test");
		assertEquals(this.expectedUser.getEmail(), user.getEmail());
	}

}
