package com.pietruh.recipes.dao.producer;

import static org.junit.Assert.assertEquals;

import java.io.File;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.EnterpriseArchive;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.pietruh.recipes.dao.api.factory.ModelFactory;
import com.pietruh.recipes.dao.api.impl.model.DefaultIngredient;
import com.pietruh.recipes.dao.api.impl.model.DefaultRecipe;
import com.pietruh.recipes.dao.api.impl.model.DefaultUser;
import com.pietruh.recipes.dao.api.model.Ingredient;
import com.pietruh.recipes.dao.api.model.Recipe;
import com.pietruh.recipes.dao.api.model.User;

@RunWith(Arquillian.class)
public class ModelFactoryProducerTest {

	@Deployment
	public static Archive<?> createArchive() {

		File[] files = Maven.resolver().loadPomFromFile("pom.xml")
				.importRuntimeDependencies().resolve().withoutTransitivity()
				.as(File.class);
		//@formatter:off
		JavaArchive jar = ShrinkWrap.create(JavaArchive.class, "test.jar")
				/* .addClass(ModelFactoryProducerBean.class) */
				.addClass(ModelFactoryProducerTest.class)
				.addAsManifestResource(EmptyAsset.INSTANCE,ArchivePaths.create("beans.xml"));
		
		//jar.as(ZipArchive.class).
		EnterpriseArchive ear = ShrinkWrap.create(EnterpriseArchive.class, "test.ear")		
				.addAsLibraries(files);
		ear.addAsLibraries(jar);
		//@formatter:on
		return ear;
	}

	@Inject
	private ModelFactory modelFactory;

	@Test
	public void defaultUser() throws Exception {
		User expected = modelFactory.createUser();
		User actual = new DefaultUser();
		assertEquals(expected, actual);
	}

	@Test
	public void defaultRecipe() throws Exception {
		Recipe recipe = modelFactory.createRecipe();
		Recipe recipe2 = new DefaultRecipe();
		assertEquals(recipe, recipe2);
	}

	@Test
	public void defaultIngridient() throws Exception {
		Ingredient ingridient = modelFactory.creatIngridient();
		Ingredient ingridient2 = new DefaultIngredient();
		assertEquals(ingridient, ingridient2);
	}

}
